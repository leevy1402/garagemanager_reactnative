import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from '@react-navigation/native';
import { StatusBar } from "react-native";
import 'react-native-gesture-handler';
import Constant from "./src/consts/Constants";
import HomeScreen from "./src/views/screens/HomeScreen"
import Login from "./src/views/screens/Login"

const Stack = createStackNavigator();

const App = () => {
    return (
        <NavigationContainer>
            <StatusBar backgroundColor={Constant.color.white}></StatusBar>
            <Stack.Navigator
                screenOptions={{ headerShown: false }}
                initialRouteName='HomeScreen'
            >
                <Stack.Screen name={Constant.screenName.Login} component={Login} />
                <Stack.Screen name={Constant.screenName.Home} component={HomeScreen} />
                {/* <Stack.Screen name='Test' component={Test} /> */}
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default App;
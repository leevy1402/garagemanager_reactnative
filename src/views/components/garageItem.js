import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import Constant from '../../consts/Constants';
import theme, { SIZES } from '../../consts/theme';
import Icon from 'react-native-vector-icons/MaterialIcons';

const spacing = SIZES.padding10;
const width = (theme.width - 4 * 10) / 2;

const IconText = ({ icon, text, color }) => {
    return (
        <View style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: SIZES.padding10,
        }}>
            <Icon name={icon} size={12} color={color}></Icon>
            <Text style={{ paddingLeft: SIZES.padding10 / 2 }}>{text}</Text>
        </View>
    )
}

const Location = ({ text }) => {
    return (
        <View style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            alignItems: 'center',
            paddingBottom: SIZES.padding10,
            paddingHorizontal: SIZES.padding10,
            color: Constant.color.primaryText
        }}>
            <Icon name={'location-pin'} size={24}></Icon>
            <Text style={{
                paddingLeft: SIZES.padding10,
                fontSize: SIZES.font12
            }}> {text}</Text>
        </View >
    )
}
const GarageItem = ({ garage }) => {
    return (
        <View style={style.item}>
            <Image source={garage.img} style={style.img}></Image>
            <Text style={style.title} numberOfLines={1}> {garage.name} </Text>
            <View style={style.icon}>
                <IconText icon={'star'} text={garage.rating} color={Constant.color.iconStar} />
                <IconText icon={'comment'} text={garage.comment} color={Constant.color.iconRed} />
                <IconText icon={'camera-alt'} text={garage.rating} color={Constant.color.iconRed} />
            </View>
            <Location text={garage.location} />
        </View>
    )
}
export default GarageItem
const style = StyleSheet.create({
    item: {
        backgroundColor: Constant.color.white,
        flexDirection: 'column',
        width: width,
        margin: spacing,
        borderRadius: SIZES.radius15,
        // justifyContent: 'center',
        // alignItems: 'center'
    },
    img: {
        height: undefined,
        width: width,
        aspectRatio: 2 / 1,
        borderTopLeftRadius: SIZES.radius15,
        borderTopRightRadius: SIZES.radius15,
    },
    title: {
        padding: SIZES.padding10 / 2,
        fontWeight: 'bold',
        paddingHorizontal: SIZES.padding10,
    },
    icon: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }
})


import React, { useState } from "react";
import { Image, StyleSheet, TouchableOpacity, View, Text, TextInput } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import Constant from "../../consts/Constants";

export default function Login({ navigation }) {
    const [choice, setChoice] = useState('');
    return (
        <View style={styles.root}>
            <LinearGradient
                colors={[Constant.color.primaryRed, Constant.color.primaryRed, Constant.color.white, Constant.color.white]}
                style={styles.gradient}>

                <View style={styles.container}>

                    <View style={{ alignItems: "center" }}>
                        <Image
                            source={Constant.image.logo}
                            style={{ width: 150, height: 70 }}
                        />
                    </View>


                    <View style={styles.wrapper}>
                        {[Constant.string.person, Constant.string.garage].map(option => (
                            <View key={option} style={styles.choice}>
                                <TouchableOpacity
                                    style={styles.outer}
                                    onPress={() => setChoice(option)}>
                                    {choice === option && <View style={styles.inner}></View>}
                                </TouchableOpacity>
                                <Text style={{ marginHorizontal: 5 }}>{option}</Text>
                            </View>
                        ))}
                    </View>

                    <View style={styles.textField}>
                        <Text style={styles.textInput}>{Constant.string.phoneNumber}</Text>
                        <TextInput style={styles.action}></TextInput>
                    </View>
                    <View style={styles.textField}>
                        <Text style={styles.textInput}>{Constant.string.password}</Text>
                        <TextInput style={styles.action}></TextInput>
                    </View>

                    <TouchableOpacity style={{ marginVertical: 20 }}>
                        <View style={[styles.button, styles.login]}>
                            <Text style={styles.text_button}>{Constant.string.signIn}</Text>
                        </View>
                    </TouchableOpacity>

                    <View style={{ flexDirection: "row", justifyContent: "space-between", marginHorizontal: 80 }}>
                        <TouchableOpacity>
                            <Image source={Constant.image.facebook}
                                style={styles.item_logo}></Image>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Image source={Constant.image.zalo}
                                style={styles.item_logo}></Image>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Image source={Constant.image.apple}
                                style={styles.item_logo}></Image>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.footer}>
                    <Text style={styles.forgot}>{Constant.string.forgetPassword}</Text>
                    <View style={{ flexDirection: "row", alignItems: "center", marginVertical: 20 }}>
                        <Text style={{ marginRight: 10 }}>{Constant.string.noAccount}</Text>
                        <TouchableOpacity style={styles.button}>
                            <Text style={styles.text_button}>{Constant.string.register}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </LinearGradient>
        </View>
    );
};

const styles = StyleSheet.create({
    login: {
        paddingVertical: 14,
        borderRadius: 10,
    },
    footer: {
        alignItems: "center",
        marginVertical: 10
    },
    forgot: {
        color: '#BC3C36'
    },
    gradient: {
        flex: 1,
        width: '100%',
    },

    item_logo: {
        width: 32,
        height: 32,
        borderRadius: 32 / 2
    },
    text_button: {
        color: '#fff',
        fontWeight: "bold",
    },
    button: {
        borderRadius: 5,
        backgroundColor: '#BC3C36',
        paddingVertical: 5,
        paddingHorizontal: 10,
        alignItems: "center",
        justifyContent: "center"
    },
    textField: {
        marginVertical: 10
    },
    textInput: {
        fontWeight: "bold"
    },
    action: {
        flexDirection: 'row',
        marginTop: 5,
        borderBottomWidth: 1,
        borderBottomColor: 'black',
        paddingBottom: 5
    },
    choice: {
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 5
    },
    root: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    container: {
        height: "80%",
        width: "80%",
        borderRadius: 16,
        padding: 16,
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        backgroundColor: '#fff',
        flexDirection: "column",
        justifyContent: "space-between",
        marginTop: 20,
        marginHorizontal: 45
    },

    inner: {
        width: 10,
        height: 10,
        backgroundColor: 'black',
        borderRadius: 10,
    },
    outer: {
        width: 15,
        height: 15,
        borderWidth: 1,
        borderRadius: 15,
        justifyContent: "center",
        alignItems: "center",
    },
    wrapper: {
        flexDirection: "row",
        justifyContent: "space-evenly",
        marginVertical: 20
    }
});
import React from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text, StatusBar, TextInput } from 'react-native';
import GarageItem from '../components/garageItem';
import garageList from '../../data/garage';
import Constant from '../../consts/Constants';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { SIZES } from '../../consts/theme';
import city from '../../data/city';
import DropdownComponent from '../components/dropdown';

const HomeScreen = () => {
    return (
        <SafeAreaView style={{
            backgroundColor: Constant.color.background,
            flex: 1,
            flexDirection: 'column'
        }}>
            {header()}
            {prestigiousPartner()}
            {garageListItem()}
        </SafeAreaView>
    );
}
const header = () => {
    return (
        <View style={styles.head}>
            <View style={styles.search}>
                <Icon name="search" size={25} color={Constant.color.grey}></Icon>
                <TextInput placeholder={Constant.string.imputSearchHome}></TextInput>
            </View>
            <View style={{ flex: 1 }}>
                <DropdownComponent data={city} />
            </View>
        </View>
    )
}
const prestigiousPartner = () => {
    return (
        <View style={{
            flexDirection: 'row',
            alignItems: 'center',
            paddingLeft: SIZES.padding10,
            margin: SIZES.padding10
        }}>
            <Text style={{
                fontWeight: 'bold',
                paddingRight: SIZES.padding10
            }}>Đối tác uy tín</Text>
            <View style={{
                position: 'relative',
                justifyContent: 'center',
                alignItems: 'center',
                width: 40,
                height: 40,
                backgroundColor: Constant.color.background,
            }}>
                <Icon name={'circle'} size={35} color={Constant.color.iconCircle}></Icon>
                <Icon
                    name={'arrow-right-alt'}
                    size={20}
                    style={{ position: 'absolute', zIndex: 99, fontWeight: 'bold', }}
                ></Icon>
            </View>
        </View>
    )
}
const garageListItem = () => {
    return (
        <FlatList
            data={garageList}
            renderItem={({ item, index }) => <GarageItem garage={item} />}
            keyExtractor={item => item.id}
            numColumns={2}
            columnWrapperStyle={styles.column}
            contentContainerStyle={styles.list}
            style={styles.container}
        />
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: Constant.color.background
    },
    column: {
        flexShrink: 1,
    },
    list: {
        justifyContent: 'space-between'
    },
    search: {
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: SIZES.radius10,
        borderColor: Constant.color.primaryRed,
        borderWidth: 1,
        flex: 2,
        marginRight: SIZES.padding10 / 2
    },
    head: {
        backgroundColor: Constant.color.white,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: SIZES.padding10,
        paddingBottom: SIZES.padding10 * 2
    }
});

export default HomeScreen;
import { Dimensions } from "react-native";
export default {
    screen: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height
    },
    screenName: {
        Home: 'HomeScreen',
        Login: 'SighUp'
    },
    color: {
        white: '#FFF',
        primaryRed: '#BC3C36',
        background: '#ecf0f1',
        grey: '#696969',
        primaryText: '#2f3542',
        iconStar: '#f9ca24',
        iconRed: '#ff5252',
        iconCircle: '#bdc3c7',
    },
    image: {
        logo: require("../../assets/images/logo.png"),
        facebook: require("../../assets/images/facebook.png"),
        zalo: require("../../assets/images/zalo.png"),
        apple: require("../../assets/images/apple.png"),
    },
    string: {
        person: 'Cá nhân',
        garage: 'Garage',
        phoneNumber: 'Số điện thoại',
        password: 'Mật khẩu',
        signIn: 'Đăng nhập',
        forgetPassword: 'Quên mật khẩu',
        noAccount: 'Chưa có tài khoản ?',
        register: 'Đăng ký',
        imputSearchHome: 'Nhập tên garage hoặc dịch vụ'
    }
}
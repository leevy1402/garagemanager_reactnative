const garageList = [
    {
        id: '1', name: 'Shinwa Pro Garage Shinwa Pro Garage ',
        img: require('../../assets/images/garage1.jpg'),
        rating: '5',
        comment: '23',
        picture: '5',
        location: '415 Võ An Ninh, Hải Châu, Đà Nẵng'
    },
    {
        id: '2', name: 'Shinwa Pro Garage Shinwa Pro Garage ',
        img: require('../../assets/images/garage2.jpg'),
        rating: '4.8',
        comment: '17',
        picture: '0',
        location: '630 Nguyễn Hữu Thọ, Cẩm Lệ, Đà Nẵng'
    },
    {
        id: '3', name: 'Shinwa Pro Garage Shinwa Pro Garage ',
        img: require('../../assets/images/garage3.jpg'),
        rating: '4.7',
        comment: '12',
        picture: '0',
        location: '415 Võ An Ninh, Hải Châu, Đà Nẵng'
    },
    {
        id: '4', name: 'Shinwa Pro Garage Shinwa Pro Garage ',
        img: require('../../assets/images/garage4.jpg'),
        rating: '4.4',
        comment: '20',
        picture: '10',
        location: '415 Võ An Ninh, Hải Châu, Đà Nẵng'
    },
]
export default garageList;